# Dorsa App

# Important points about the project:

Considering the single-page nature of the project, few auxiliary tools have been used. For example, for websites with such content, Next.js framework is a better option.
The RTK/Query library has been used for caching requests.

## Available Scripts

For the first time, install dependencies

### `npm install`

In the project directory, you can run:

### `npm start`

## Live Preview

Check it here [Demo](https://facebook.github.io/create-react-app/docs/getting-started).
