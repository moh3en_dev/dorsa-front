import React, { useState } from "react";

interface Props {
  onChange: (option: string) => void;
}

function SortOptions({ onChange }: Props) {
  const [selectedOption, setSelectedOption] = useState("rate");

  const handleOptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedOption(event?.target.value);
    onChange(event?.target.value);
  };

  return (
    <div className="radio-group">
      <label>
        <input
          type="radio"
          value="rate"
          checked={selectedOption === "rate"}
          onChange={handleOptionChange}
        />
        <span className="radio-circle"></span>
        <span className="radio-label">بیشترین امتیاز</span>
      </label>
      <label>
        <input
          type="radio"
          value="view"
          checked={selectedOption === "view"}
          onChange={handleOptionChange}
        />
        <span className="radio-circle"></span>
        <span className="radio-label">بیشترین بازدید</span>
      </label>
      <label>
        <input
          type="radio"
          value="newest"
          checked={selectedOption === "newest"}
          onChange={handleOptionChange}
        />
        <span className="radio-circle"></span>
        <span className="radio-label">جدیدترین</span>
      </label>
    </div>
  );
}

export default SortOptions;
