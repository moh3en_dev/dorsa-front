import { useEffect, useRef, useState } from "react";
import { Link } from "react-router-dom";
import Card from "src/components/card";
import CircleLoader from "src/components/circle-loader";
import { useGetMoviesQuery } from "./home-slice";
import SortOptions from "./sort-options";
import { IMovie } from "src/models/movie";

export default function App() {
  const scrollableDivRef = useRef<HTMLDivElement>(null);
  const [page, setPage] = useState(1);
  const [sort, setSort] = useState("rate");
  const [opened, setOpened] = useState("");
  const [showBackToTop, setShowBackToTop] = useState(false);
  const { data: movies = [], isFetching } = useGetMoviesQuery({ sort, page });

  function handleSort() {
    setOpened((prevState) => (prevState !== "opened" ? "opened" : ""));
  }

  function handleBackToTop() {
    const scrolledDiv = scrollableDivRef.current;
    if (scrolledDiv) {
      scrolledDiv.scrollTo({ top: 0, behavior: "smooth" });
    }
  }

  function handleScroll() {
    const scrollableDiv = scrollableDivRef.current;
    if (
      scrollableDiv &&
      scrollableDiv.scrollHeight - scrollableDiv.scrollTop ===
        scrollableDiv.clientHeight
    ) {
      setPage((prevPage) => prevPage + 1);
    }
    if (scrollableDiv) {
      if (scrollableDiv.scrollTop > 100) {
        setShowBackToTop(true);
      } else {
        setShowBackToTop(false);
      }
    }
  }

  useEffect(() => {
    const scrollableDiv = scrollableDivRef.current;

    if (scrollableDiv) {
      scrollableDiv.addEventListener("scroll", handleScroll);
    }

    return () => {
      if (scrollableDiv) {
        scrollableDiv.removeEventListener("scroll", handleScroll);
      }
    };
  }, []);

  return (
    <div className="container">
      <p className="back">
        <button onClick={handleBackToTop}>
          <img
            src={`${process.env.PUBLIC_URL}/assets/icons/arrow_right.svg`}
            alt="back"
          />
          <span className="pr-4 text-sm text-gray">بازگشت</span>
        </button>
      </p>
      <div className="head-nav">
        <div className="flex-col">
          <span className="font-bold">چیارو ببینه؟</span>
          <span className="text-sm text-gray">مناسب برای ۳ تا ۷ سال</span>
        </div>
        <div>
          <button onClick={handleSort}>
            <img
              src={`${process.env.PUBLIC_URL}/assets/icons/sort.svg`}
              alt="sort"
            />
            <span className="pr-4">مرتب سازی</span>
          </button>
        </div>
      </div>
      <div className="content" ref={scrollableDivRef}>
        <div className="movie-list">
          {movies.length > 0 ? (
            movies.map((movie: IMovie, i: number) => (
              <Link to="/" key={i}>
                <Card movie={movie} />
              </Link>
            ))
          ) : (
            <p>-</p>
          )}
        </div>
      </div>
      {isFetching && <CircleLoader />}
      <div
        className={`dark-overlay ${opened}`}
        onClick={() => setOpened("")}
      ></div>
      <div className="sort-box" onClick={() => setOpened("")}>
        <div className="open">
          <div className={`box ${opened} `}>
            <p className="sort-by">مرتب سازی بر اساس</p>
            <div className="sort-options">
              <SortOptions onChange={setSort} />
            </div>
          </div>
        </div>
      </div>
      {showBackToTop && (
        <button className="back-to-top" onClick={handleBackToTop}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={2}
            stroke="#fff"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M4.5 15.75l7.5-7.5 7.5 7.5"
            />
          </svg>
        </button>
      )}
    </div>
  );
}
