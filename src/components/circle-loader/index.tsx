function CircleLoader() {
  return (
    <div className="loading-overlay">
      <div className="loading-spinner" />
    </div>
  );
}

export default CircleLoader;
