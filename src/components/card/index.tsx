import { IMovie } from "src/models/movie";

type Props = {
  movie: IMovie;
};

function Card({ movie }: Props) {
  return (
    <div className="movie-item">
      <div className="movie-poster-container">
        <img
          className="movie-poster"
          src={movie?.reviewsThumbnailUrl}
          alt={movie?.reviewsTitle}
        />
      </div>
      <h3 className="movie-title">{movie?.reviewsTitle}</h3>
      <p className="movie-rating">
        <img
          src={`${process.env.PUBLIC_URL}/assets/icons/star.svg`}
          alt="sort"
        />
        <span className="pr-4">{movie?.reviewsRate}</span>
      </p>
    </div>
  );
}

export default Card;
