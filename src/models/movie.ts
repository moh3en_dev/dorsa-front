export type IMovie = {
  reviewsThumbnailUrl: string;
  reviewsTitle: string;
  reviewsRate: string;
};
